﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllDataSheetFinder
{
    public enum PartMoreInfoState
    {
        NotAvailable,
        Downloading,
        Available,
    }
}
